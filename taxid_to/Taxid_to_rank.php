<?php
header('Content-Type: text/plain;charset=UTF-8');
//ini_set('auto_detect_line_endings', 1);
ini_set('error_reporting', E_ALL);
date_default_timezone_set('Asia/Tokyo');
$timeHeader = date("Y-m-d_H-i-s");

$file = "";
if (count($argv) > 0){
  $file = $argv[1];
}

$write_file = $file.'_output.csv'; //書き込みファイル
$err_file = $file.'_err.txt'; //書き込みファイル

if(is_file($file)) {
    $lines = array();
    try {
        $filedata = file_get_contents($file);
        $str = str_replace(array("\r\n","\r","\n"), "\n", $filedata);
        $lines = explode("\n", $str);
    }
    catch (Exception $e) {
        error_log($e);
        $err_result = file_put_contents($err_file, $e, LOCK_EX| FILE_APPEND); //ロック指定可能
    }
    if (count($lines) > 1){
        $csv_header = $lines[0]; 
        $csv_body = array_splice($lines, 1);
    }
    else {
        exit;
    }

    $file_data = "taxId,rank,scientificName,formalName,division,geneticCode,submittable,lineage";
    $count = 1;
    foreach ($csv_body as $row) {
        echo "count:".$count.PHP_EOL;
        $count++;
        try {
            $row_array = explode(',', $row);
            if (count($row_array)>0){
                $taxid = $row_array[0];
            }
            $url = 'https://www.ebi.ac.uk/ena/taxonomy/rest/tax-id/'.$taxid;
            $json = file_get_contents($url);
            //echo $json;
            $array = json_decode($json,true);

            $taxId = "";
            $scientificName = "";
            $formalName = "";
            $rank = "";
            $division = "";
            $lineage = "";
            $geneticCode = "";
            $submittable = "";

            if (isset($array["taxId"])){
                $taxId = $array["taxId"];    
            }
            
            if (isset($array["scientificName"])){
                $scientificName = $array["scientificName"];
            }
            if (isset($array["formalName"])){
                $formalName = $array["formalName"];
            }
            if (isset($array["rank"])){
                $rank = $array["rank"];
            }
            if (isset($array["division"])){
                $division = $array["division"];
            }
            if (isset($array["lineage"])){
                $lineage = $array["lineage"];
            }
            if (isset($array["geneticCode"])){
                $geneticCode = $array["geneticCode"];
            }
            if (isset($array["submittable"])){
                $submittable = $array["submittable"];
            }

            $file_data = $taxId.",".$rank.",".$scientificName.",".$formalName.",".$division.",".$geneticCode.",".$submittable.",".$lineage.PHP_EOL;
            $write_result = file_put_contents($write_file, $file_data, LOCK_EX| FILE_APPEND); //ロック指定可能

            /*
            echo $taxId.PHP_EOL;
            echo $scientificName.PHP_EOL;
            echo $formalName.PHP_EOL;
            echo $rank.PHP_EOL;
            echo $division.PHP_EOL;
            echo $lineage.PHP_EOL;
            echo $geneticCode.PHP_EOL;
            echo $submittable.PHP_EOL;
            echo "----".PHP_EOL;
            */

        }
        catch (Exception $e) {
            error_log($e);
            $err_result = file_put_contents($err_file, $e, LOCK_EX| FILE_APPEND); //ロック指定可能
        }
    }           
}
?>